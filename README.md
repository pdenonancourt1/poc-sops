# Using SOPS for secrets in Gitlab CI

We want to create and regularly update the following secret:

```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: application-secret
  annotations:
    experimental: "true"
  labels:
    environment: preprod
data:
  PASSWORD: c2VjcmV0LXBhc3N3b3JkCg==
  TOKEN: c2VjcmV0LXRva2VuCg==
```

Since the values here are simply base64 encoded, it is not recommended to commit this file in case
some gets access to the repository since it can easily get decoded (`echo <value> | base64 -d`).

To be able to version this file, we can create an encrypted version of the secret using [sops](https://github.com/getsops/sops).

## Create the encrypted secret

Assuming the original unencrypted secret is the `secret.yaml` file, we can use GPG encryption:

```
sops --encrypt --pgp <PGP-ID> --encrypted-regex='^(data)$' secret.yaml > sops-secret.yaml
```

This will return a file that can usually be safely committed.

```yaml
apiVersion: v1
kind: Secret
metadata:
    name: application-secret
    annotations:
        experimental: "true"
    labels:
        environment: preprod
data:
    PASSWORD: ENC[AES256_GCM,data:L7XUswMcmST/DysyYW6k6KMWif2PnD2+,iv:cJyKwSvguJqx16EZsRNBNuZkSxhkLqFZVJ9SyiofqF4=,tag:dra4tl8ps0LMJp8ng0RUqQ==,type:str]
    TOKEN: ENC[AES256_GCM,data:dL7QEEyqliUwDkgh4niZD2a1K38=,iv:K87WDNlBTxWEt5/3sGYJY821J+WH8rbJZWQ8SizztdE=,tag:t07pV6MB/SRlQqqaEFZf6w==,type:str]
sops:
    # Details about how the file is encrypted
```

## Update the secret

If we have the permissions to decrypt the secret and we want to update it (add or update a value):

```
sops sops-secret.yaml
```

It will use the default `EDITOR` but we can open the file in VSCode if needed:

```
EDITOR=code\ --wait sops sops-secret.yaml
```

## Apply the secret to Kubernetes

To get the decrypted value of our secret, we can do:

```
sops -d sops-secret.yaml
```

Our Gitlab CI job needs to have the permissions to decrypt the secret to be able to apply it.
In our case, we provide the private PGP key so that Gitlab CI is able to decrypt the secret.

```
sops -d sops-secret.yaml | kubectl apply -f -
```

This way, the unencrypted secret will never be committed.

## Other

- SOPS is used here for demonstration purposes. Any other encryption mechanism may be used as long as Gitlab CI jobs may have access to a way to decrypt files.
- If using SOPS, use cloud providers key management services (GCP KMS / AWS KMS / etc) and give permissions to Gitlab CI jobs to decrypt files using that instead of relying on local files.
- Build a dedicated image with `sops`, `kubectl` and other required tools pre-installed.
